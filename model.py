from torch import nn
from model_resnet import Generator
import torch
import numpy as np
from gensim.models import FastText
from torchvision.utils import save_image


class GenFromText(nn.Module):
    def __init__(self,
                 g_model_path,
                 t_model_path,
                 z_dim, chn):
        super().__init__()
        self.G = Generator(z_dim, None, chn=chn)
        self.z_dim = z_dim
        G_state = torch.load(g_model_path,
                             map_location=lambda storage, location: storage)
        self.G.load_state_dict(G_state)
        self.G.cuda()
        self.text_model = FastText.load_fasttext_format(t_model_path)

    def forward(self, text, nsamples):
        word_vec = []
        for w in text.lower().strip().split():
            vec = (self.text_model.wv[w]).copy()
            word_vec.append(vec.reshape(1,-1))
        word_mat = np.concatenate(word_vec, 0)
        phrase_vec = np.mean(word_mat, axis=0).astype(np.float32)
        phrase_vec /= np.sqrt(np.sum(phrase_vec**2)+1e-10)
        phrase_mat = np.tile(phrase_vec, (nsamples,0))

        emb = torch.from_numpy(phrase_mat).cuda()
        z = torch.randn(nsamples, self.z_dim, dtype=torch.float32).cuda()
        z *= 0.5
        return self.G.forward(z, emb)