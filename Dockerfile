FROM nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04

ARG PYTHON_VERSION=3.6

# binary dependencies on ubuntu
RUN apt-get update && apt-get install -y --no-install-recommends \
         build-essential zip unzip \
         cmake default-jre \
         git \
         curl \
         vim \
         ca-certificates \
         libjpeg-dev \
         libpng-dev openssh-server \
         sox libsox-dev libsox-fmt-all \
         zlib1g-dev automake autoconf libtool subversion libatlas3-base \
         wget python python-dev \
         pkg-config libprotobuf9v5 protobuf-compiler libprotobuf-dev \
         libgoogle-perftools-dev libprotobuf9v5 \
         build-essential libboost-all-dev cmake zlib1g-dev \
         libbz2-dev liblzma-dev &&\
     rm -rf /var/lib/apt/lists/*

# pytorch
RUN curl -o ~/miniconda.sh -O \
    https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh  && \
     chmod +x ~/miniconda.sh && \
     ~/miniconda.sh -b -p /opt/conda && \
     rm ~/miniconda.sh && \
     /opt/conda/bin/conda install -y python=$PYTHON_VERSION numpy pyyaml \
     scipy ipython mkl mkl-include cython typing && \
     /opt/conda/bin/conda install -y -c pytorch magma-cuda90 && \
     /opt/conda/bin/conda clean -ya
ENV PATH /opt/conda/bin:$PATH

RUN pip install --upgrade pip

RUN conda install pytorch torchvision -c pytorch

# python packages
RUN pip install torchvision gensim \
    scipy tensorflow tensorboardX flask Jinja2

WORKDIR /workspace
RUN chmod -R a+w /workspace
ENV LANG C.UTF-8
