#!/usr/bin/python
import sys
import argparse
import cairosvg
import os
import multiprocessing

def convert(pair):
    print(str.format(' {} => {}', pair[0], pair[1]))
    cairosvg.svg2png(url=pair[0], write_to=pair[1], parent_width=64, parent_height=64)
    #cairosvg.svg2png(url=pair[0], write_to=pair[1])
    #cairosvg.svg2png(url='a', write_to='b')

def create_list(inputDir, outputDir):
    ans = []
    for fname in os.listdir(inputDir):
        if fname.endswith('.svg'):
  
            newfile = str.format('{}png',fname[:-3])
            newfile = os.path.join(outputDir, newfile)
            oldfile = os.path.join(inputDir, fname)
            ans.append((oldfile,newfile))
    return ans

def parse_cmdline():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("inputDir",
        help="input directory")
    arg_parser.add_argument("outputDir",
        help="output directory")
    return arg_parser.parse_args()

if __name__ == "__main__":
    args = parse_cmdline()
    pairlist = create_list(args.inputDir, args.outputDir)
    num_cpus = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes = num_cpus)
    pool.map(convert, pairlist)
