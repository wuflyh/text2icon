from flask import Flask, render_template, request, g
import random
import os
import time
import shutil
from model import GenFromText
from torchvision.utils import save_image


app = Flask(__name__)
g_model_path = "gen" # gan model path
t_model_path = "fast" # fast text model path
nsamples = 10

@app.route('/',methods = ['POST', 'GET'])
def index():
   model = loadModel()
   if request.method == 'POST':
      name = request.form['Name']
      name = name.strip()
      if len(name) == 0:
          return render_template("index.html")
      result = {'name':name}
      sessionId = str(int(time.time()*1000))
      result['images'] = renderImgs(sessionId, name, model)
      return render_template("index.html",result = result)
   else:
      return render_template("index.html")

def loadModel():
    if not hasattr(g, 'model'):
        rootDir = os.path.dirname(os.path.abspath(__file__))
        g_model_path = os.path.join(rootDir, g_model_path)
        t_model_path = os.path.join(rootDir, t_model_path)
        g.model = GenFromText(
                 g_model_path,
                 t_model_path,
                 120, 64)
        print('loading once')
    return g.model

def renderImgs(sessionId, inputText, model):
    urls = []
    rootDir = os.path.dirname(os.path.abspath(__file__))
    rootDir = os.path.join(rootDir, 'static')
    dst = os.path.join(rootDir, sessionId)

    gen_images = g.model.forward(inputText, nsamples)
    gen_images = (gen_images+1)/2.0*0.95 # try not too saturate
    for i in range(nsamples):
        file_name = "image_{}.png".format(i)
        full_file_name = os.path.join(dst, file_name)
        save_image((gen_images+1)/2, full_file_name)
        url = '/'.join(['static', sessionId, file_name])
        urls.append(url)
    return urls

    """
    src = os.path.join(rootDir, 'images')
    os.makedirs(dst)
    src_files = os.listdir(src)
    for file_name in src_files:
        full_file_name = os.path.join(src, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name, dst)
            url = '/'.join(['static', sessionId, file_name])
            urls.append(url)
    """

if __name__ == '__main__':
   app.run(debug = True)
